﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace TicTacToe.DesktopApp.Models
{
    /// <summary>
    /// Represents field mark.
    /// </summary>
    [DebuggerDisplay("{Name}")]
    public class Mark : IEquatable<Mark>
    {
        /// <summary>
        /// Gets the circle mark.
        /// </summary>
        public static Mark Circle => new Mark(nameof(Circle), "○");

        /// <summary>
        /// Gets the cross mark.
        /// </summary>
        public static Mark Cross => new Mark(nameof(Cross), "X");

        /// <summary>
        /// Gets the empty mark.
        /// </summary>
        public static Mark Empty => new Mark(nameof(Empty), string.Empty);

        private Mark(string name, string content)
        {
            Content = content ?? throw new ArgumentNullException(nameof(content));
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        /// <summary>
        /// Gets the string representation of current mark.
        /// </summary>
        public string Content { get; }

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current mark
        /// is equal to <see cref="Circle"/>.
        /// </summary>
        public bool IsCircle => Equals(Circle);

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current mark
        /// is equal to <see cref="Cross"/>.
        /// </summary>
        public bool IsCross => Equals(Cross);

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current mark
        /// is empty.
        /// </summary>
        public bool IsEmpty => string.Empty == Content;

        /// <summary>
        /// Gets the mark name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Determines whether value of the specified <see cref="Mark"/>
        /// is equal to the current <see cref="Mark"/>.
        /// </summary>
        /// <param name="other">
        /// The <see cref="Mark"/> to compare with the current <see cref="Mark"/>.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="Mark"/>
        /// is equal to the current <see cref="Mark"/>;
        /// otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(Mark other)
        {
            return other != null
                && other.Name == Name
                && other.Content == Content;
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified object is equal
        /// to the current object; otherwise, <see langword="false"/>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Equals(obj as Mark);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            return 1997410482 + EqualityComparer<string>.Default.GetHashCode(Content);
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the current mark.
        /// </summary>
        /// <returns>
        /// <see cref="string"/> representation of current mark.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }

        public static bool operator ==(Mark mark1, Mark mark2)
        {
            return EqualityComparer<Mark>.Default.Equals(mark1, mark2);
        }

        public static bool operator !=(Mark mark1, Mark mark2)
        {
            return !(mark1 == mark2);
        }
    }
}
