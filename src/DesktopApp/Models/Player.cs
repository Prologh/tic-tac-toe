﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace TicTacToe.DesktopApp.Models
{
    /// <summary>
    /// Represents game player.
    /// </summary>
    [DebuggerDisplay("{PlayerType}")]
    public class Player : IEquatable<Player>
    {
        /// <summary>
        /// Gets the computer player.
        /// </summary>
        public static Player Computer => new Player(nameof(Computer), Mark.Cross, PlayerType.Computer, startsTheTurn: false);

        /// <summary>
        /// Gets the human player.
        /// </summary>
        public static Player You => new Player(nameof(You), Mark.Circle, PlayerType.Human, startsTheTurn: true);

        private Player(string name, Mark mark, PlayerType playerType, bool startsTheTurn)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException(
                    $"{name} cannot be null, empty or contain only whitespaces.",
                    nameof(name));
            }

            Mark = mark;
            Name = name;
            PlayerType = playerType;
            StartsTheTurn = startsTheTurn;
        }

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current player
        /// is computer controller.
        /// </summary>
        public bool IsComputer => PlayerType == PlayerType.Computer;

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current player
        /// is human controller.
        /// </summary>
        public bool IsHuman => PlayerType == PlayerType.Human;

        /// <summary>
        /// Gets the mark assigned to the player.
        /// </summary>
        public Mark Mark { get; }

        /// <summary>
        /// Gets the player name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the player type.
        /// </summary>
        public PlayerType PlayerType { get; }

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current player
        /// starts the game turn.
        /// </summary>
        public bool StartsTheTurn { get; }

        /// <summary>
        /// Determines whether value of the specified <see cref="Player"/>
        /// is equal to the current <see cref="Player"/>.
        /// </summary>
        /// <param name="other">
        /// The <see cref="Player"/> to compare with the current <see cref="Player"/>.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="Player"/>
        /// is equal to the current <see cref="Player"/>;
        /// otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(Player other)
        {
            return other != null
                && other.Mark == Mark
                && other.Name == Name
                && other.PlayerType == PlayerType
                && other.StartsTheTurn == StartsTheTurn;
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified object is equal
        /// to the current object; otherwise, <see langword="false"/>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            var hashCode = -878118430;
            hashCode = hashCode * -1521134295 + Mark.GetHashCode();
            hashCode = hashCode * -1521134295 + PlayerType.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + StartsTheTurn.GetHashCode();

            return hashCode;
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the current player.
        /// </summary>
        /// <returns>
        /// <see cref="string"/> representation of current player.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }

        public static bool operator ==(Player player1, Player player2)
        {
            return EqualityComparer<Player>.Default.Equals(player1, player2);
        }

        public static bool operator !=(Player player1, Player player2)
        {
            return !(player1 == player2);
        }
    }
}
