﻿namespace TicTacToe.DesktopApp.Models
{
    /// <summary>
    /// Indicates the type of player.
    /// </summary>
    public enum PlayerType
    {
        Human = 0,
        Computer
    }
}
