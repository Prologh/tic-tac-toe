﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TicTacToe.DesktopApp.Models
{
    /// <summary>
    /// Represents field position.
    /// </summary>
    [DebuggerDisplay("{Name,nq} ({ShortName,nq})")]
    public class Position : IEquatable<Position>
    {
        /// <summary>
        /// Get the field position has not yet been set.
        /// </summary>
        public static Position Unset => new Position(0, nameof(Unset));

        /// <summary>
        /// Get the field position is set to north-west (NW) corner.
        /// </summary>
        public static Position NorthWest => new Position(1, nameof(NorthWest));

        /// <summary>
        /// Get the field position is set to north (N) side.
        /// </summary>
        public static Position North => new Position(2, nameof(North));

        /// <summary>
        /// Get the field position is set to north-east (NE) corner.
        /// </summary>
        public static Position NorthEast => new Position(4, nameof(NorthEast));

        /// <summary>
        /// Get the field position is set to west (W) side.
        /// </summary>
        public static Position West => new Position(8, nameof(West));

        /// <summary>
        /// Get the field position is set to board center.
        /// </summary>
        public static Position Center => new Position(16, nameof(Center));

        /// <summary>
        /// Get the field position is set to east (E) side.
        /// </summary>
        public static Position East => new Position(32, nameof(East));

        /// <summary>
        /// Get the field position is set to south-west (SW) corner.
        /// </summary>
        public static Position SouthWest => new Position(64, nameof(SouthWest));

        /// <summary>
        /// Get the field position is set to south (S) side.
        /// </summary>
        public static Position South => new Position(128, nameof(South));

        /// <summary>
        /// Get the field position is set to south-west (SW) corner.
        /// </summary>
        public static Position SouthEast => new Position(256, nameof(SouthEast));

        private Position(int score, string name)
        {
            Score = score;
            Name = name;
        }

        /// <summary>
        /// Gets the position name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current position
        /// is equal to <see cref="Unset"/>.
        /// </summary>
        public bool IsUnset => this == Unset;

        /// <summary>
        /// Gets the 32-bit signed integer representation of position.
        /// </summary>
        public int Score { get; }

        /// <summary>
        /// Gets the short version of position name.
        /// </summary>
        /// <example>
        /// NorthWest ==> NW
        /// </example>
        public string ShortName => new string(Name.Where(c => char.IsUpper(c)).ToArray());

        /// <summary>
        /// Determines whether value of the specified <see cref="Position"/>
        /// is equal to the current <see cref="Position"/>.
        /// </summary>
        /// <param name="other">
        /// The <see cref="Position"/> to compare with the current <see cref="Position"/>.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="Position"/>
        /// is equal to the current <see cref="Position"/>;
        /// otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(Position other)
        {
            return other != null &&
                   Name == other.Name &&
                   Score == other.Score;
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the current position.
        /// </summary>
        /// <returns>
        /// <see cref="string"/> representation of current position.
        /// </returns>
        public override string ToString()
        {
            return $"{Name} ({ShortName})";
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified object is equal
        /// to the current object; otherwise, <see langword="false"/>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Equals(obj as Position);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            var hashCode = 192700399;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + Score.GetHashCode();

            return hashCode;
        }

        public static bool operator ==(Position position1, Position position2)
        {
            return EqualityComparer<Position>.Default.Equals(position1, position2);
        }

        public static bool operator !=(Position position1, Position position2)
        {
            return !(position1 == position2);
        }
    }
}
