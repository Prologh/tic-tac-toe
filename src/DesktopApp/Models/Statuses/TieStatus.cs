﻿namespace TicTacToe.DesktopApp.Models.Statuses
{
    /// <summary>
    /// Represents a tie game status.
    /// </summary>
    public class TieStatus : GameStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TieStatus"/> class.
        /// </summary>
        public TieStatus() : base(GameEndingType.Tie)
        {

        }
    }
}
