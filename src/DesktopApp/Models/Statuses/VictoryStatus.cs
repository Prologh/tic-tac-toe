﻿using System.Collections.Generic;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Models.Statuses
{
    /// <summary>
    /// Represents victory game status.
    /// </summary>
    public class VictoryStatus : GameStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VictoryStatus"/> class.
        /// </summary>
        /// <param name="winningFields">
        /// A collection of fields being a direct cause of game end.
        /// </param>
        /// <param name="winningPlayer">
        /// The player who won the game.
        /// </param>
        public VictoryStatus(IEnumerable<FieldVM> winningFields, Player winner) : base(GameEndingType.Victory, winningFields, winner)
        {

        }
    }
}
