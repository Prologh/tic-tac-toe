﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TicTacToe.DesktopApp.Models
{
    /// <summary>
    /// Represents a set of <see cref="Position"/>s forming together
    /// a winning pattern.
    /// </summary>
    [DebuggerDisplay("\\{ Score = {Score}, Count = {Positions.Count()}\\}")]
    public class WinningPattern : IEquatable<WinningPattern>, IEquatable<ISet<Position>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WinningPattern"/> class.
        /// </summary>
        /// <param name="positions">
        /// A set of possitions forming a winning pattern.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="positions"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// <paramref name="positions"/> are empty.
        /// </exception>
        public WinningPattern(ISet<Position> positions)
        {
            if (positions == null)
            {
                throw new ArgumentNullException(nameof(positions));
            }

            if (!positions.Any()){
                throw new ArgumentException(
                    "A winning pattern can be formed out of at least one position.");
            }

            Positions = positions ?? throw new ArgumentNullException(nameof(positions));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WinningPattern"/> class.
        /// </summary>
        /// <param name="positions">
        /// A set of possitions forming a winning pattern.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="positions"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// <paramref name="positions"/> are empty.
        /// </exception>
        public WinningPattern(params Position[] positions)
        {
            if (positions == null)
            {
                throw new ArgumentNullException(nameof(positions));
            }

            if (!positions.Any())
            {
                throw new ArgumentException(
                    "A winning pattern can be formed out of at least one position.");
            }

            Positions = new HashSet<Position>(positions);
        }

        /// <summary>
        /// Gets the positions set.
        /// </summary>
        public ISet<Position> Positions { get; }

        /// <summary>
        /// Gets the pattern score, that is a sum of every position score.
        /// </summary>
        public int Score => Positions.Sum(position => position.Score);

        /// <summary>
        /// Determines whether value of the specified <see cref="WinningPattern"/>
        /// is equal to the current <see cref="WinningPattern"/>.
        /// </summary>
        /// <param name="other">
        /// The <see cref="WinningPattern"/> to compare with the current
        /// <see cref="WinningPattern"/>.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="WinningPattern"/>
        /// is equal to the current <see cref="WinningPattern"/>;
        /// otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(WinningPattern other)
        {
            return other != null
                && Score == other.Score;
        }

        /// <summary>
        /// Determines whether value of the specified <see cref="ISet{}"/> of
        /// <see cref="Position"/>s is equal to the current <see cref="WinningPattern"/>.
        /// </summary>
        /// <param name="other">
        /// The <see cref="ISet{}"/> of <see cref="Position"/>s to compare
        /// with the current <see cref="WinningPattern"/>.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="ISet{}"/>
        /// of <see cref="Position"/>s is equal to the current
        /// <see cref="WinningPattern"/>; otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(ISet<Position> other)
        {
            return other != null
                && Score == other.Sum(position => position.Score);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified object is equal
        /// to the current object; otherwise, <see langword="false"/>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Equals(obj as WinningPattern);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            var hashCode = -2036685432;
            hashCode = hashCode * -1521134295 + EqualityComparer<ISet<Position>>.Default.GetHashCode(Positions);
            hashCode = hashCode * -1521134295 + Score.GetHashCode();

            return hashCode;
        }

        public static bool operator ==(WinningPattern pattern1, WinningPattern pattern2)
        {
            return EqualityComparer<WinningPattern>.Default.Equals(pattern1, pattern2);
        }

        public static bool operator !=(WinningPattern pattern1, WinningPattern pattern2)
        {
            return !(pattern1 == pattern2);
        }

    }
}
