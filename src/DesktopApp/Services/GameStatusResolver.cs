﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicTacToe.DesktopApp.Models.Statuses;
using TicTacToe.DesktopApp.ViewModels;

namespace TicTacToe.DesktopApp.Services
{
    public class GameStatusResolver : IGameStatusResolver
    {
        private readonly IPlayerManager _playerManager;
        private readonly IWinningPatternsRepository _patternsRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameStatusResolver"/> class.
        /// </summary>
        /// <param name="playerManager">
        /// The player manager serivce.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="playerManager"/> is <see langword="null"/>.
        /// </exception>
        public GameStatusResolver(IPlayerManager playerManager)
        {
            _playerManager = playerManager ?? throw new ArgumentNullException(nameof(playerManager));
            _patternsRepository = new WinningPatternsRepository();
        }

        /// <summary>
        /// Gets current game status.
        /// </summary>
        /// <returns>
        /// An instance of <see cref="GameStatus"/>.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// Related <see cref="Board.Fields"/> collection is empty.
        /// </exception>
        public GameStatus Resolve(IEnumerable<FieldVM> fields)
        {
            if (fields == null)
            {
                throw new ArgumentNullException(nameof(fields));
            }

            if (!fields.Any())
            {
                throw new InvalidOperationException(
                    "Cannot operate on empty fields collection.");
            }

            if (fields.All(field => field.HasEmptyMark))
            {
                return new OngoingStatus();
            }

            foreach (var pattern in _patternsRepository.WinningPatterns)
            {
                var patternFields = fields.Where(field => pattern.Positions.Contains(field.Position));
                if (!patternFields.Any())
                {
                    var formattedPattern = string.Join("\n", pattern.Positions.Select(p => p.ToString()));
                    throw new InvalidOperationException(
                        "Cannot find fields with position suitable for current pattern:\n\n" +
                        formattedPattern);
                }

                if (patternFields.Any(f => f.HasEmptyMark))
                {
                    continue;
                }

                foreach (var player in _playerManager.Players)
                {
                    if (patternFields.All(field => field.Mark == player.Mark))
                    {
                        return new VictoryStatus(patternFields, player);
                    }
                }
            }

            if (fields.All(field => !field.HasEmptyMark))
            {
                return new TieStatus();
            }

            return new OngoingStatus();
        }
    }
}
