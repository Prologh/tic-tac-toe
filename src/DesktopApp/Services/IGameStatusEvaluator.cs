﻿using TicTacToe.DesktopApp.Models;
using TicTacToe.DesktopApp.Models.Statuses;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Defines minimum requirements for games status evaluating service.
    /// </summary>
    public interface IGameStatusEvaluator
    {
        /// <summary>
        /// Returns calculated games status value.
        /// </summary>
        /// <param name="gameStatus">
        /// The game status to evaluate.
        /// </param>
        /// <param name="currentPlayer">
        /// The player holding the current turn torwards whom
        /// the evaluation should be made.
        /// </param>
        /// <returns>
        /// Game status value.
        /// </returns>
        int EvaluateGameStatus(GameStatus gameStatus, Player currentPlayer);
    }
}
