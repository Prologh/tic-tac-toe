﻿using System.Windows.Media;
using TicTacToe.DesktopApp.Models;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Provides helper methods related to <see cref="Mark"/> enumeration.
    /// </summary>
    public class MarkHelper : IMarkHelper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MarkHelper"/> class.
        /// </summary>
        public MarkHelper()
        {

        }

        /// <summary>
        /// Gets enabled <see cref="bool"/> value corresponding to provided
        /// <see cref="Mark"/>.
        /// </summary>
        /// <param name="mark">
        /// The field <see cref="Mark"/>.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> value.
        /// </returns>
        public bool GetMarkIsEnabled(Mark mark)
        {
            return mark == Mark.Empty;
        }

        /// <summary>
        /// Gets foreground brush corresponding to provided <see cref="Mark"/>.
        /// </summary>
        /// <param name="mark">
        /// The <see cref="Mark"/>.
        /// </param>
        /// <returns>
        /// An instance of <see cref="Brush"/>.
        /// </returns>
        public Brush GetMarkForeground(Mark mark)
        {
            if (mark == Mark.Cross)
            {
                return new SolidColorBrush(Colors.LightGray);
            }
            else
            {
                if (mark == Mark.Circle)
                {
                    return new SolidColorBrush(Colors.DodgerBlue);
                }
                else
                {
                    return new SolidColorBrush(Colors.White);
                }
            }
        }
    }
}
