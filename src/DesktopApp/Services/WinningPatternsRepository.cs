﻿using System.Collections.Generic;
using TicTacToe.DesktopApp.Models;

namespace TicTacToe.DesktopApp.Services
{
    /// <summary>
    /// Provides winning patters of field positions.
    /// </summary>
    public class WinningPatternsRepository : IWinningPatternsRepository
    {
        private readonly ISet<WinningPattern> _positions;

        /// <summary>
        /// Initializes a new instance of the <see cref="WinningPatternsRepository"/>
        /// class.
        /// </summary>
        public WinningPatternsRepository()
        {
            _positions = CreateWinningPatterns();
        }

        /// <summary>
        /// Gets the winning patterns.
        /// </summary>
        public ISet<WinningPattern> WinningPatterns => _positions;

        private ISet<WinningPattern> CreateWinningPatterns()
        {
            var patterns = CreateDiagonalWinningPatterns();
            patterns.UnionWith(CreateHorizontalWinningPatterns());
            patterns.UnionWith(CreateVerticalWinningPatterns());

            return patterns;
        }

        private ISet<WinningPattern> CreateDiagonalWinningPatterns()
        {
            return new HashSet<WinningPattern>
            {
                // Left top corenr to right bottom corner - diagonal line.
                new WinningPattern
                (
                    Position.NorthWest,
                    Position.Center,
                    Position.SouthEast
                ),

                // Left bottom corner to right top corner - diagonal line.
                new WinningPattern
                (
                    Position.SouthWest,
                    Position.Center,
                    Position.NorthEast
                ),
            };
        }

        private ISet<WinningPattern> CreateHorizontalWinningPatterns()
        {
            return new HashSet<WinningPattern>
            {
                // Top horizontal line.
                new WinningPattern
                (
                    Position.NorthWest,
                    Position.North,
                    Position.NorthEast
                ),

                // Center horizontal line.
                new WinningPattern
                (
                    Position.West,
                    Position.Center,
                    Position.East
                ),

                // Bottom horizontal line.
                new WinningPattern
                (
                    Position.SouthWest,
                    Position.South,
                    Position.SouthEast
                ),
            };
        }

        private ISet<WinningPattern> CreateVerticalWinningPatterns()
        {
            return new HashSet<WinningPattern>
            {
                // Left vertical line.
                new WinningPattern
                (
                    Position.NorthWest,
                    Position.West,
                    Position.SouthWest
                ),

                // Center vertical line.
                new WinningPattern
                (
                    Position.North,
                    Position.Center,
                    Position.South
                ),

                // Right vertical line.
                new WinningPattern
                (
                    Position.NorthEast,
                    Position.East,
                    Position.SouthEast
                ),
            };
        }
    }
}
